var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    var num1=parseInt(req.query.number1);
    var num2=parseInt(req.query.number2);   
    var sight=parseInt(req.query.sight);
    var ans;
    if (sight==1) {
        ans=num1+num2;
    }    
    else if (sight==2) {
        ans=num1-num2;
    }
    else if (sight==3) {
        ans=num1*num2;
    }
    else if (sight==4) {
        ans=num1/num2;
    }
    else {
        ans=num1%num2;
    }
    res.render('calculator', { answer: ans });
});

module.exports = router;
